//
//  ICECustomScrollView.h
//  HungryHunter
//
//  Created by Vitalis on 06.11.2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTUIManager.h>

@interface ICECustomScrollView : UICollectionView<UIGestureRecognizerDelegate>

@property (nonatomic, assign) BOOL centerContent;

@end
