//
//  ICESnapScrollView.m
//  HungryHunter
//
//  Created by Vitalis on 06.11.2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "ICESnapScrollViewManager.h"

@implementation ICESnapScrollViewManager

RCT_EXPORT_MODULE()

- (UIView *)view
{
  return [[ICESnapScrollView alloc] initWithEventDispatcher:self.bridge.eventDispatcher];
}

RCT_EXPORT_VIEW_PROPERTY(itemWidth, CGFloat)
RCT_EXPORT_VIEW_PROPERTY(itemHeight, CGFloat)
RCT_EXPORT_VIEW_PROPERTY(padding, CGFloat)
RCT_EXPORT_VIEW_PROPERTY(itemsMargin, CGFloat)
RCT_EXPORT_VIEW_PROPERTY(selectedItemIndex, int)
RCT_EXPORT_VIEW_PROPERTY(values, NSArray)
RCT_EXPORT_VIEW_PROPERTY(tapticEnabled, BOOL)

RCT_EXPORT_VIEW_PROPERTY(onChange, RCTBubblingEventBlock)


@end
