//
//  ICESnapScrollView.h
//  HungryHunter
//
//  Created by Vitalis on 06.11.2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <React/RCTView.h>
#import <UIKit/UIKit.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTAssert.h>
#import <React/UIView+React.h>
#import "ICECustomScrollView.h"
#import "ItemCell.h"


@interface ICESnapScrollView : RCTView <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>


- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;

/**
 * The `RCTScrollView` may have at most one single subview. This will ensure
 * that the scroll view's `contentSize` will be efficiently set to the size of
 * the single subview's frame. That frame size will be determined somewhat
 * efficiently since it will have already been computed by the off-main-thread
 * layout system.
 */
@property (nonatomic, readonly) UIView *contentView;

/**
 * If the `contentSize` is not specified (or is specified as {0, 0}, then the
 * `contentSize` will automatically be determined by the size of the subview.
 */
@property (nonatomic, assign) CGSize contentSize;

/**
 * The underlying scrollView (TODO: can we remove this?)
 */
@property (nonatomic, readonly) UIScrollView *collectionView;

@property (nonatomic, assign) CGFloat itemWidth;
@property (nonatomic, assign) CGFloat itemHeight;
@property (nonatomic, assign) CGFloat padding;
@property (nonatomic, assign) CGFloat itemsMargin;
@property (nonatomic, assign) int selectedItemIndex;

@property (nonatomic, assign) NSArray * items;
@property NSArray * values;
@property BOOL tapticEnabled;

@property (nonatomic, copy) RCTBubblingEventBlock onChange;

@end
