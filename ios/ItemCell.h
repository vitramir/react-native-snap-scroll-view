//
//  ItemCell.h
//  HungryHunter
//
//  Created by Vitalis on 09.11.2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UICollectionViewCell

    @property UIView * view;

@end
