
Pod::Spec.new do |s|
  s.name         = "ICESnapScrollView"
  s.version      = "1.0.0"
  s.summary      = "ICESnapScrollView"
  s.description  = <<-DESC
                  ICESnapScrollView
                   DESC
  s.homepage     = "https://github.com/vitramir/react-native-snap-scrollview"
  s.license      = "MIT"
  s.author             = { "author" => "vitalis@icedigital.ru" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/vitramir/react-native-snap-scrollview.git#master", :tag => "master" }
  s.source_files  = "*.{h,m}"
  s.public_header_files   = '*.h'

  s.requires_arc = true

  s.dependency "React"

end
