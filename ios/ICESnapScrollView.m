//
//  ICESnapScrollView.m
//  HungryHunter
//
//  Created by Vitalis on 06.11.2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "ICESnapScrollView.h"

@implementation ICESnapScrollView
{
  RCTEventDispatcher *_eventDispatcher;
  ICECustomScrollView *_scrollView;
  UIView *_contentView;
  NSMutableArray * items;
  UICollectionViewFlowLayout * _layout;
  int lastSelectedItem;
    UIImpactFeedbackGenerator * impactFeedback;
    BOOL isDragging;
}


- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher
{
  RCTAssertParam(eventDispatcher);
  
  if ((self = [super initWithFrame:CGRectZero])) {
    _eventDispatcher = eventDispatcher;
    
    items=[NSMutableArray new];
    
    _layout=[UICollectionViewFlowLayout new];
    _layout.itemSize=CGSizeMake(10, 200);
    _layout.scrollDirection=UICollectionViewScrollDirectionHorizontal;
    
    _scrollView=[[ICECustomScrollView alloc] initWithFrame:CGRectZero collectionViewLayout:_layout];
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _scrollView.delegate = self;
    _scrollView.dataSource=self;
    _scrollView.delaysContentTouches = NO;
    _scrollView.backgroundColor=[UIColor clearColor];
    
    _scrollView.showsHorizontalScrollIndicator=NO;
    _scrollView.showsVerticalScrollIndicator=NO;
//    _scrollView.alpha=0.5;
    [_scrollView registerClass:[ItemCell class] forCellWithReuseIdentifier:@"item_cell"];
    
      lastSelectedItem=0;
    
    [self addSubview:_scrollView];
  }
  return self;
}

-(void)setItemWidth:(CGFloat)itemWidth{
  _itemWidth=itemWidth;
  [_layout setItemSize:CGSizeMake(_itemWidth, _itemHeight)];
}

-(void)setItemHeight:(CGFloat)itemHeight{
  _itemHeight=itemHeight;
  [_layout setItemSize:CGSizeMake(_itemWidth, _itemHeight)];
}

-(void)setPadding:(CGFloat)padding{
  _padding=padding;
  [_scrollView setContentInset:UIEdgeInsetsMake(0, _padding, 0, _padding)];
  [_scrollView setContentOffset:CGPointMake(-_padding, 0)];
}

-(void)setItemsMargin:(CGFloat)itemsMargin{
  _itemsMargin=itemsMargin;
  [_layout setMinimumLineSpacing:itemsMargin];
  [_layout setMinimumInteritemSpacing:itemsMargin];
}

-(void)setSelectedItemIndex:(int)selectedItemIndex{
    _selectedItemIndex=selectedItemIndex;
    lastSelectedItem=selectedItemIndex;
    if(!isDragging){
        CGFloat snapInterval=_itemWidth+_itemsMargin;
        _scrollView.contentOffset=CGPointMake(snapInterval*selectedItemIndex-_padding, 0);
    }
}


static inline void RCTApplyTranformationAccordingLayoutDirection(UIView *view, UIUserInterfaceLayoutDirection layoutDirection) {
  view.transform =
  layoutDirection == UIUserInterfaceLayoutDirectionLeftToRight ?
  CGAffineTransformIdentity :
  CGAffineTransformMakeScale(-1, 1);
}

//- (CGSize)contentSize
//{
//  if (!CGSizeEqualToSize(_contentSize, CGSizeZero)) {
//    return _contentSize;
//  }
//  return _contentView.frame.size;
//}

- (void)reactBridgeDidFinishTransaction
{
  CGSize contentSize = self.contentSize;
  if (!CGSizeEqualToSize(_scrollView.contentSize, contentSize)) {
    // When contentSize is set manually, ScrollView internals will reset
    // contentOffset to  {0, 0}. Since we potentially set contentSize whenever
    // anything in the ScrollView updates, we workaround this issue by manually
    // adjusting contentOffset whenever this happens
//    CGPoint newOffset = [self calculateOffsetForContentSize:contentSize];
//    _scrollView.contentSize = contentSize;
    
//    _scrollView.contentOffset = newOffset;
  }
}


- (void)insertReactSubview:(UIView *)view atIndex:(NSInteger)atIndex
{
  
  [super insertReactSubview:view atIndex:atIndex];
    [items insertObject:view atIndex:atIndex];
    [_scrollView reloadData];
}


- (void)removeReactSubview:(UIView *)subview
{
    [super removeReactSubview:subview];
  {
      [items removeObject:subview];
      [_scrollView reloadData];
  }
}



- (void)layoutSubviews
{
  [super layoutSubviews];
  RCTAssert(self.subviews.count == 1, @"we should only have exactly one subview");
  RCTAssert([self.subviews lastObject] == _scrollView, @"our only subview should be a scrollview");
}

- (void)didUpdateReactSubviews
{
  // Do nothing, as subviews are managed by `insertReactSubview:atIndex:`
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    isDragging=YES;
    if(_tapticEnabled)
    {
        impactFeedback=[UIImpactFeedbackGenerator new];
        [impactFeedback prepare];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
  
  CGFloat snapInterval=_itemWidth+_itemsMargin;
  (*targetContentOffset).x=round((targetContentOffset->x+_padding)/snapInterval)*snapInterval-_padding;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
        CGFloat snapInterval=_itemWidth+_itemsMargin;
        float pos=(_scrollView.contentOffset.x+_padding)/snapInterval;
        bool toLeft=pos < lastSelectedItem;
        int item=toLeft?ceil(pos):floor(pos);
        if(lastSelectedItem!=item){
            lastSelectedItem=item;
            if(_tapticEnabled){
                [impactFeedback impactOccurred];
            }
        }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate){
        [self sendOnScrolledToItem];
        impactFeedback=nil;
        isDragging=NO;
    }
}
         
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self sendOnScrolledToItem];
    impactFeedback=nil;
    isDragging=NO;
}

-(void)sendOnScrolledToItem{
    if(_onChange!=nil){
        CGFloat snapInterval=_itemWidth+_itemsMargin;
        int item=round((_scrollView.contentOffset.x+_padding)/snapInterval);
        NSMutableDictionary * event=[[NSMutableDictionary alloc] initWithDictionary: @{@"item":@(item)}];
        if(_values!=nil && [_values count]>item){
            event[@"value"]=_values[item];
        }
        _onChange(event);
    }
}




-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
  return [items count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
  ItemCell * cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"item_cell" forIndexPath:indexPath];
    if([cell.subviews count]>0)
    {
        [cell.subviews[0] removeFromSuperview];
    }
    if([items count]>indexPath.row){
        cell.view=items[indexPath.row];
       [cell addSubview:cell.view];
    }
  return cell;
}


@end
