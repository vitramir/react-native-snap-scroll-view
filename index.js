import { View } from 'react-native';

import React, { useState } from 'react';

import SnapScrollView from './scroll';

const initSate = {
  layout: false,
  width: 0,
  height: 0,
  padding: 0,
  itemWidth: 0,
  totalWidth: 0,
  snapInterval: 0,
};

export default ({ ...props }) => {
  const [scrollState, setScrollState] = useState(initSate);

  const onLayout = event => {
    let { width, height } = event.nativeEvent.layout;

    let { itemsOnScreen, itemsMargin, onCalculateSize, padding } = props;

    let itemWidth = Math.round(
      (width - padding * 2 - (itemsOnScreen - 1) * itemsMargin) / itemsOnScreen
    );

    if (itemWidth < 0) itemWidth = 0;
    let snapInterval = itemWidth + itemsMargin;

    if (onCalculateSize) {
      onCalculateSize({ itemWidth });
    }
    setScrollState({ layout: true, width, height, itemWidth, snapInterval });
  };

  return (
    <View onLayout={onLayout} style={props.style}>
      <SnapScrollView
        itemWidth={scrollState.itemWidth}
        itemHeight={scrollState.height}
        snapInterval={scrollState.snapInterval}
        layout={scrollState.layout}
        {...props}
      />
    </View>
  );
};
