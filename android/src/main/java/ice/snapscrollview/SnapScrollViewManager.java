package ice.snapscrollview;

import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ScrollView;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.ArrayList;

/**
 * Created by Vitalis on 07.11.2017.
 */

public class SnapScrollViewManager extends ViewGroupManager<SnapScrollView> {

    public static final String REACT_CLASS = "ICESnapScrollView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public SnapScrollView createViewInstance(ThemedReactContext context) {
        return new SnapScrollView(context);
    }

    @Override
    public void addView(SnapScrollView parent, View child, int index) {
        parent.addItem(child, index);
    }

    @Override
    public int getChildCount(SnapScrollView parent) {
        return parent.getViewCountInAdapter();
    }

    @Override
    public View getChildAt(SnapScrollView parent, int index) {
        return parent.getViewFromAdapter(index);
    }

    @Override
    public void removeView(SnapScrollView parent, View view) {
        parent.removeItem(view);
    }


    @Override
    public void removeViewAt(SnapScrollView parent, int index) {
//        Log.i("ICE", "remove view2");
        parent.removeItem(index);
    }

    @Override
    public void removeAllViews(SnapScrollView parent) {
//        super.removeAllViews(parent);
    }


    @ReactProp(name = "itemWidth", defaultInt = 0)
    public void setItemWidth(SnapScrollView view, int itemWidth) {
        view.setItemWidth(getDPSize(view, itemWidth));
    }


    @ReactProp(name = "itemsMargin", defaultInt = 0)
    public void setItemsMargin(SnapScrollView view, int margin) {
        view.setItemsMargin(getDPSize(view, margin/2));
    }

    @ReactProp(name = "padding", defaultInt = 0)
    public void setPadding(SnapScrollView view, int padding) {
        view.setPadding(getDPSize(view, padding));
    }

    @ReactProp(name = "values")
    public void setValues(SnapScrollView view, ReadableArray values) {
        view.setValues(values.toArrayList());
    }

    @ReactProp(name = "selectedItemIndex", defaultInt = -1)
    public void setSelectedItemIndex(SnapScrollView view, int index) {
        view.setSelectedItemIndex(index);
    }

    @ReactProp(name = "androidSelectedOffset", defaultInt = 0)
    public void setAndroidSelectedOffset(SnapScrollView view, int offset) {
        view.setSelectedOffset(offset);
    }



    int getDPSize(View view, int size){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, view.getResources().getDisplayMetrics());
    }

}
