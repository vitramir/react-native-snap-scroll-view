package ice.snapscrollview;

import android.content.Context;
import android.graphics.Rect;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.RelativeLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.MeasureSpecAssertions;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vitalis on 07.11.2017.
 */

public class SnapScrollView extends RecyclerView implements GravitySnapHelper.SnapListener {
    private SnapAdapter mAdapter;
    private LinearLayoutManager layoutManager;
    Context context;
    RecyclerView recyclerView=this;

    int itemWidth=0;
    int itemsMargin=0;
    int padding=0;
    int selectedOffset=0;

    int selectedItemIndex=-1;

    ArrayList itemValues;

    public SnapScrollView(Context context) {
        super(context);

        layoutManager=new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        this.setLayoutManager(layoutManager);
        this.context=context;

        List<View> input = new ArrayList<>();
        mAdapter = new SnapAdapter(input);
        this.setAdapter(mAdapter);

        GravitySnapHelper snapHelper = new GravitySnapHelper(Gravity.START, false, this);
        snapHelper.attachToRecyclerView(this);

        this.setClipToPadding(false);
    }

    public void addItem(View view, int index){
        mAdapter.addItem(view, index);
    }

    public void removeItem(View view){
        mAdapter.removeItem(view);
    }

    public void removeItem(int index){
        mAdapter.removeItem(index);
    }

    public int getViewCountInAdapter(){
        return mAdapter.values.size();
    }

    public View getViewFromAdapter(int index){
        return mAdapter.values.get(index);
    }

    public void setItemWidth(int width){
        itemWidth=width;
        mAdapter.setItemWidth(width);
    }

    public void setPadding(int padding){
        if(this.padding!=padding){
            this.padding=padding;
            this.setPadding(padding-itemsMargin,0,padding-itemsMargin,0);
        }
    }

    public void setItemsMargin(int margin){
        if(itemsMargin!=margin){
            itemsMargin=margin;
            this.addItemDecoration(new ItemOffsetDecoration(margin));

            setPadding(padding);
        }
    }

    public void setValues(ArrayList values){
        this.itemValues=values;
    }

    public void setSelectedOffset(int offset) {
        selectedOffset=offset;
    }


    public void setSelectedItemIndex(int index) {
        if(index!=-1 && selectedItemIndex!=index){
            if(index>selectedItemIndex){
                layoutManager.scrollToPosition(index+4); /////+4 ???
            }
            else
            {
                layoutManager.scrollToPosition(index);
            }
            smoothScrollToPosition(index+selectedOffset);//bug on android in some cases
            selectedItemIndex=index;
        }
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        MeasureSpecAssertions.assertExplicitMeasureSpec(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    public void onSnap(int position) {
        WritableMap event = Arguments.createMap();
        event.putInt("item", position);
        if(itemValues!=null && itemValues.size()>position){
            event.putString("value", (String)itemValues.get(position));
        }
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
                getId(),
                "topChange",
                event);

    }


    public class SnapAdapter extends RecyclerView.Adapter<SnapAdapter.ViewHolder> {
        private List<View> values;
        int itemWidth = 0;


        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public RelativeLayout layout;

            public ViewHolder(RelativeLayout v) {
                super(v);
                layout = v;
//                txtHeader = (TextView) v.findViewById(R.id.firstLine);
//                txtFooter = (TextView) v.findViewById(R.id.secondLine);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public SnapAdapter(List<View> myDataset) {
            values = myDataset;
        }

        public void addItem(View v, int index){
            values.add(index, v);
            notifyDataSetChanged();
        }

        public void removeItem(View v){
            values.remove(v);
            notifyDataSetChanged();
        }

        public void removeItem(int index){
            values.remove(values.get(index));
            notifyDataSetChanged();
        }

        public void setItemWidth(int width){
            if(itemWidth!=width) {
                itemWidth = width;
                mAdapter.notifyDataSetChanged();
            }
        }

        // Create new views (invoked by the layout manager)
        @Override
        public SnapAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

            // set the view's size, margins, paddings and layout parameters
            RelativeLayout v =new RelativeLayout(context);
            v.setLayoutParams(new FrameLayout.LayoutParams(itemWidth, ViewGroup.LayoutParams.MATCH_PARENT));

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.layout.removeAllViews();
            View v=values.get(position);

            RelativeLayout parent=(RelativeLayout) v.getParent();
            if(parent!=null) {
                parent.removeView(v);
            }
            holder.layout.addView(v);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            int count=(itemWidth==0)?0:values.size();
            return count;
        }

    }



    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

}
