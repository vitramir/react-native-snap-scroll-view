import { requireNativeComponent, View } from 'react-native';

import React from 'react';

const ICESnapScrollView = requireNativeComponent('ICESnapScrollView', null);

export default ({ itemWidth, itemHeight, snapInterval, layout, ...props }) => {
  let {
    items,
    renderItem,
    itemsMargin,
    padding,
    onScrolledToItem,
    values,
    selectedItemIndex,
    androidSelectedOffset,
    captureMove = false,
  } = props;

  let itemStyle = {
    width: itemWidth,
    height: itemHeight,
    position: 'absolute',
  };

  if (!layout) return null;
  return (
    <ICESnapScrollView
      {...(captureMove
        ? {
            onMoveShouldSetResponderCapture: evt =>
              Math.abs(evt.nativeEvent.pageX - this._startLocation) > 10,
          }
        : {})}
      {...(captureMove
        ? {
            onStartShouldSetResponderCapture: evt => {
              this._startLocation = evt.nativeEvent.pageX;
              return false;
            },
          }
        : {})}
      style={{ flex: 1 }}
      onChange={event => onScrolledToItem(event.nativeEvent)}
      {...{
        itemWidth,
        itemHeight,
        padding,
        itemsMargin,
        selectedItemIndex,
        androidSelectedOffset,
        values,
      }}
    >
      {items.map((item, i) => (
        <View style={itemStyle} key={i}>
          {renderItem(item)}
        </View>
      ))}
    </ICESnapScrollView>
  );
};
